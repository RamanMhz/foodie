<?php
	require 'foodieHeader.php';
	session_start();
	$_SESSION['rid'] = null;
	$keywords = mysqli_real_escape_string(mysqli_connect("localhost", "root", "", "foodie"), $_GET['q']);
	printf('<script>var keywords = "%s";</script>', $keywords);
?>
<script>
	var search_result_lower_limit = 0;
	function show_search_result(lower_limit = 0) {
		$.ajax({
			url: "foodie-execute.php",
			method: "POST",
			data: {show_search_result: 1, keywords: keywords, lower_limit: lower_limit},
			success: function(data){
				$(".search-result-list").append(data);
				if(data == '') {
					$("#show-more-search-result").hide();
				}
			}
		});
	}
	$(document).on('ready', function(){
		show_search_result();
		$("#show-more-search-result").click(function(){
			search_result_lower_limit += 8;
			show_search_result(search_result_lower_limit);
		})
	});
</script>
<style>	
	* {
		background-repeat: no-repeat;
	}

	body{
		/*background-image: url('img/loginbg.jpg');*/
		background-image: url('img/img-foodie-image@3x.jpg');
		background-attachment: fixed;
		background-position: center;
		background-size: cover;
		background-repeat: no-repeat;
		/*height: 4000px;*/
		background-color: #fff;
	}

</style>
<style >
	@media (max-width: 576px){
		.related-post > div, .most-viewed-post > div, .search-result > div {
			display: inline-flex;
		}
	}
	.related-post, .most-viewed-post, .search-result {
		border: solid #f8f9fa 5px;
		border-width: 5px 8px;
	}
	.foodie-post-image {
		width: 100%;
		min-height: 140px;
	}
</style>
<body>
<!-- TopNavbar -->
<?php require 'foodienav.php'; ?>			
<!-- /.TopNavbar -->
<!-- SignUp Form -->
<div id="signform-div" class="d-none row" style="top:0">
	<div id="signbg" style="position:absolute;height: 100%;width: 100%; background-color: rgba(0,0,255,0.3);">
	</div>
	<form class="form-signin pb-4 col-md-3" id="form-signin" action="foodieSignin.inc.php" method="POST">
		<img class="mb-4" src="img/foodie.png" alt="" width="100" height="72">
		<h1 class="h3 mb-3 font-weight-normal font-warning">Please sign in</h1>
		<div id="SignUpErrorBox" class="text-danger"><?php if(isset($_GET['loginerror'])) echo $_GET['loginerror']; ?></div>
		<label for="inputEmail" class="sr-only">Email address</label>
		<input title="Email address or Username" type="text" name="email" id="inputEmail" class="form-control" placeholder="Email address or Username" required>
		<label for="inputPassword" class="sr-only">Password</label>
		<input title="Password" type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
		<div class="checkbox mb-2">
			<label>
				<input type="checkbox" value="remember-me"> Remember me
			</label>
		</div>
		<button class="btn btn-lg btn-warning btn-block mb-3" type="submit" name="Signin">Sign in</button>
		<label for="foodieSignup_link">No Foodie Account?</label>
		<a class="link mb-2" id="foodieSignup_link" href="foodieSignup.php">Click here to SignUp Now!!</a>
		<a class="link mt-3 d-block" href="forgotPwd.php">Forgot Password?</a>

		<p class="text-muted m-0 mt-2 p-0" style="cursor:default;line-height: normal">&copy; 2020-2021</p>
	</form>
</div>
<!-- /.SignUp Form -->
<div id="topContainer" class="container-fluid row m-0 p-0 pb-4 bg-light">
	<div class="d-block bg-light text-center py-3 w-100"></div>
	<article id="content" class="col-md-12 m-0 p-0 px-4 pl-md-5">
		<section class="m-0" id="search-result">
			<h3 id="search-result-title" class="search-result-title d-inline display-6">Search Result</h3>
			<hr class="mt-1 mb-0 bg-warning">
			<div class="search-result-list row mt-2 mx-0">
				<!-- search result filled by AJAX -->
			</div>
			<button class="btn btn-light float-right" id="show-more-search-result" style="border-bottom-color: grey; border-right-color: grey" >More Recipes..</button>
		</section>
	</article>
</div>
<footer class="blog-footer bg-dark text-light text-center p-2">
  <div class="mb-n4 text-left"><a id="BackToTop" href="#Top" class="text-light smooth-scroll">&uparrow;Back to top </a></div>
  Foodie's World created for foodies
  <div>&copy; 2020 Copyright FoodiesWorld.com</div>
</footer>
</body>
<?php 
if(!isset($_SESSION['foodieuserid'])){
	// script for Login button or Account button in navbar
	echo "
		<script>
			$(function() {
					$('.nav-link.Account').html('Login');
					$('.nav-link.Account').attr('id','loginbtn');
				});
		</script>
		";
	}
else {
	echo "
		<script>
			$(function() {
					$('.nav-link.Account').html('Account');
					$('.nav-link.Account').attr('id','accountbtn');
					$('.nav-link.Account').attr('data-toggle','dropdown')
				});
		</script>
		";
	}
	// script forLogin button or Account button in navbar ends

if(isset($_GET['loginerror'])){
	echo "<script> $(function() {show_SignUp_Form(); })</script>";
}
?>
</html>