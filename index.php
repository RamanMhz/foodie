<?php
	require 'foodieHeader.php';
	session_start();
	$_SESSION['rid'] = null;
?>
<script>
	var recent_posts_count = 0;
	$(document).on('ready', function(){
		$("#show-more-recent-posts-home").click();
		$("#show-more-recent-posts-home").click();
		$("#show-more-most-viewed-posts-home").click();
		$("#show-more-most-viewed-posts-home").click();
		
	});
</script>
<style>	
	* {
		background-repeat: no-repeat;
	}

	body{
		/*background-image: url('img/loginbg.jpg');*/
		background-image: url('img/img-foodie-image@3x.jpg');
		background-attachment: fixed;
		background-position: center;
		background-size: cover;
		background-repeat: no-repeat;
		/*height: 4000px;*/
		background-color: #fff;
	}

	#navbarFoodie.show {
		background: rgba(15, 17, 16, 0.5);
	}

	.navbar-dark .navbar-nav .nav-link {
		color: rgba(255, 255, 255, 0.8);
		/*color: #ffffff99;*/
		/*transition: background 1s ease-in-out;*/

	}

	.navbar-dark .navbar-nav .nav-link:hover {
		color: #fff;
		padding-bottom: 6px;	/* initial: 8px */
		border-bottom: 2px solid var(--green-apple);
		border-bottom: 2px solid var(--teal);
		border-bottom: 2px solid #ffc107;
		background: #0002; 
		/*transition: background 0.5s ease-in-out;*/
	}

	#navbarFoodie > div > a.nav-item.active.nav-link {
		opacity: 1;
		/*color: green;*/
		/*color: var(--green-apple);*/
		/*color: #28a745;*/
		color: #2f3;
	}

</style>
<style >
	@media (max-width: 576px){
		.recent-post > div, .most-viewed-post > div {
			display: inline-flex;
		}
	}
</style>
</head>
<body>
<!-- TopNavbar -->
<?php require 'foodienav.php'; ?>		
<!-- /.TopNavbar -->
<!-- SignUp Form -->
<div id="signform-div" class="d-none row" style="top:0">
	<div id="signbg" style="position:absolute;height: 100%;width: 100%; background-color: rgba(0,0,255,0.3);">
	</div>
	<form class="form-signin pb-4 col-md-3" id="form-signin" action="foodieSignin.inc.php" method="POST">
		<img class="mb-4" src="img/foodie.png" alt="" width="100" height="72">
		<h1 class="h3 mb-3 font-weight-normal font-warning">Please sign in</h1>
		<div id="SignUpErrorBox" class="text-danger"><?php if(isset($_GET['loginerror'])) echo $_GET['loginerror']; ?></div>
		<label for="inputEmail" class="sr-only">Email address</label>
		<input title="Email address or Username" type="text" name="email" id="inputEmail" class="form-control" placeholder="Email address or Username" required>
		<label for="inputPassword" class="sr-only">Password</label>
		<input title="Password" type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
		<div class="checkbox mb-2">
			<label>
				<input type="checkbox" value="remember-me"> Remember me
			</label>
		</div>
		<button class="btn btn-lg btn-warning btn-block mb-3" type="submit" name="Signin">Sign in</button>
		<label for="foodieSignup_link">No Foodie Account?</label>
		<a class="link mb-2" id="foodieSignup_link" href="foodieSignup.php">Click here to SignUp Now!!</a>
		<a class="link mt-3 d-block" href="forgotPwd.php">Forgot Password?</a>

		<p class="text-muted m-0 mt-2 p-0" style="cursor:default;line-height: normal">&copy; 2020-2021</p>
	</form>
</div>
<!-- /.SignUp Form -->
<div id="topContainer" class="container-fluid row m-0 p-0 pb-4 bg-light">
	<div class="d-block bg-light text-center py-3 w-100"></div>
	<article id="content" class="col-md-12 m-0 p-0 px-4 pl-md-5">
		<section class="m-0" id="recent-post">
			<h3 id="recent-post-title" class="recent-post-title d-inline display-6">Recent Recipes</h3>
			<hr class="mt-1 mb-0 bg-warning">
			<div class="recent-posts-list row mt-2 mx-0">

		</div>
		<button class="btn btn-light float-right" id="show-more-recent-posts-home" style="border-bottom-color: grey; border-right-color: grey" >More Recent Recipes..</button>
		<!-- ./Recent Recipes ends -->

		<!-- Most viewed Recipes starts -->		

		<section class="m-0 mt-5" id="most-viewed-post">
			<h3 id="most-viewed-post-title" class="most-viewed-post-title d-inline display-6">Most Viewed Recipes</h3>
			<hr class="mt-1 mb-0 bg-warning">
		<div class="most-viewed-posts-list row mt-2 mx-0">
		</div>
		<button class="btn btn-light float-right" id="show-more-most-viewed-posts-home" style="border-bottom-color: grey; border-right-color: grey">More Similar Posts..</button>
	</article>
</div>

<footer class="blog-footer bg-dark text-light text-center p-2">
  <div class="mb-n4 text-left"><a id="BackToTop" href="#Top" class="text-light smooth-scroll">&uparrow;Back to top </a></div>
  Foodie's World created for foodies
  <div>&copy; 2020 Copyright FoodiesWorld.com</div>
</footer>
</body>
<?php 
if(!isset($_SESSION['foodieuserid'])){
	// script for Login button or Account button in navbar
	echo "
		<script>
			$(function() {
					$('.nav-link.Account').html('Login');
					$('.nav-link.Account').attr('id','loginbtn');
				});
		</script>
		";
	}
else {
	echo "
		<script>
			$(function() {
					$('.nav-link.Account').html('Account');
					$('.nav-link.Account').attr('id','accountbtn');
					$('.nav-link.Account').attr('data-toggle','dropdown')
				});
		</script>
		";
	}
	// script forLogin button or Account button in navbar ends

if(isset($_GET['loginerror'])){
	echo "<script> $(function() {show_SignUp_Form(); })</script>";
}
?>
</html>